﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Circle : Point
    {
        public Circle()
        {
                
        }
        public Circle(int x,int y, double r)
            :base(x,y)
        {
            this.R = r;
        }

        public double R
        {
            get { return R; }
            set
            {
                if (value <= 0)
                    R = 1;
                else
                    R = value;
            }
        }



    }

}

