﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Cylinder:Circle
    {
        public Cylinder() { }
        public Cylinder(int x,int y,int r, int h)
            :base(x,y,r)
        {
            this.H = h; 
        }

        public int H;
    }
}
